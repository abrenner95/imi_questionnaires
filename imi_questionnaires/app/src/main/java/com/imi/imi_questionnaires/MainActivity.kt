package com.imi.imi_questionnaires

import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.quickbirdstudios.surveykit.*
import com.quickbirdstudios.surveykit.result.TaskResult
import com.quickbirdstudios.surveykit.steps.CompletionStep
import com.quickbirdstudios.surveykit.steps.InstructionStep
import com.quickbirdstudios.surveykit.steps.QuestionStep
// import com.quickbirdstudios.surveykit.*
import com.quickbirdstudios.surveykit.survey.SurveyView
import org.apache.commons.csv.CSVFormat
import java.nio.file.Files
import java.nio.file.Paths
import java.text.SimpleDateFormat
import java.util.*
import org.apache.commons.csv.CSVPrinter

class MainActivity : AppCompatActivity() {

    protected lateinit var survey: SurveyView
    private lateinit var container: ViewGroup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        survey = findViewById(R.id.survey_view)
        container = findViewById(R.id.surveyContainer)
        setupSurvey(survey)
    }

    private fun setupSurvey(surveyView: SurveyView) {
        val steps = listOf(
            InstructionStep(
                title = this.resources.getString(R.string.intro_title),
                text = this.resources.getString(R.string.intro_text),
                buttonText = this.resources.getString(R.string.intro_start)
            ),
            QuestionStep(
                title = this.resources.getString(R.string.q1_text),
                text = "",
                answerFormat = AnswerFormat.SingleChoiceAnswerFormat(
                    textChoices = listOf(
                        TextChoice(this.resources.getString(R.string.q1_option1)),
                        TextChoice(this.resources.getString(R.string.q1_option2)),
                        TextChoice(this.resources.getString(R.string.q1_option3)),
                        TextChoice(this.resources.getString(R.string.q1_option4))
                    )
                )
            ),
            QuestionStep(
                title = this.resources.getString(R.string.q2_text),
                text = "",
                answerFormat = AnswerFormat.SingleChoiceAnswerFormat(
                    textChoices = listOf(
                        TextChoice(this.resources.getString(R.string.q2_option1)),
                        TextChoice(this.resources.getString(R.string.q2_option2)),
                        TextChoice(this.resources.getString(R.string.q2_option3)),
                        TextChoice(this.resources.getString(R.string.q2_option4))
                    )
                )
            ),
            CompletionStep(
                title = this.resources.getString(R.string.finish_question_title),
                text = this.resources.getString(R.string.finish_question_text),
                buttonText = this.resources.getString(R.string.finish_question_submit)
            )
        )

        val task = OrderedTask(steps = steps)

        surveyView.onSurveyFinish = { taskResult: TaskResult, reason: FinishReason ->
            if (reason == FinishReason.Completed) {
                taskResult.results.forEach { stepResult ->
                    Log.e("logTag", "answer ${stepResult.results.firstOrNull()}")
                    // container.removeAllViews()
                    storeCSV()
                }
            }
        }

        val configuration = SurveyTheme(
            themeColorDark = ContextCompat.getColor(this, R.color.colorPrimaryDark),
            themeColor = ContextCompat.getColor(this, R.color.colorPrimary),
            textColor = ContextCompat.getColor(this, R.color.colorPrimary)
        )

        surveyView.start(task, configuration)
    }

    private fun storeCSV() {
        val formatter = SimpleDateFormat("yyyy_MM_dd HH:mm:ss", Locale.US);
        val date = Date()
        val csvFilepath = ("storage/emulated/0/Documents/Surveys/" + formatter.format(date)+ ".csv")

        val writer = Files.newBufferedWriter(Paths.get(csvFilepath))
        val csvPrinter = CSVPrinter(writer, CSVFormat.DEFAULT
            .withHeader("ID", "Name", "Designation", "Company"))
        csvPrinter.printRecord("1", "karthiq", "admin", "chercher tech")
        csvPrinter.printRecord("2", "Jeff", "CEO", "Amazon")
        csvPrinter.printRecord("3", "Tim", "CEO", "Apple")
        csvPrinter.flush()
        csvPrinter.close()
    }
}
